package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.api.service.IUserService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.UserNotFoundException;
import ru.t1.volkova.tm.exception.field.EmailEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.LoginEmptyException;
import ru.t1.volkova.tm.exception.field.PasswordEmptyException;
import ru.t1.volkova.tm.exception.user.LoginExistsException;
import ru.t1.volkova.tm.exception.user.RoleEmptyException;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.service.*;

import java.sql.SQLException;
import java.util.List;


public class UserServiceTest {

    @Nullable
    private List<UserDTO> userList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, projectService, taskService, propertyService);

    @Before
    public void initRepository() throws SQLException {
        userList = userService.findAll();
    }

    @Test
    public void testCreate(
    ) throws Exception {
        int size = userService.getSize();
        @Nullable final UserDTO user = userService.create("new_login", "password332");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(size + 1, userService.getSize());
    }

    @Test
    public void testCreateWithEmail(
    ) throws Exception {
        int size = userService.getSize();
        @Nullable final UserDTO user = userService.create("new_login2", "password332", "new_user@mail.ru");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(size + 1, userService.getSize());
    }

    @Test
    public void testCreateWithRole(
    ) throws Exception {
        int size = userService.getSize();
        @Nullable final UserDTO user = userService.create("usual3", "password332", Role.USUAL);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(size + 1, userService.getSize());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateLoginEmpty(
    ) throws SQLException {
        userService.create(null, "password332");
        userService.create("", "password332");
    }

    @Test(expected = LoginExistsException.class)
    public void testCreateLoginExists(
    ) throws SQLException {
        userService.create("user", "password332");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreatePasswordEmpty(
    ) throws SQLException {
        userService.create("new_user", null);
        userService.create("new_user", "");
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateEmailEmpty(
    ) throws SQLException {
        userService.create("new_login4", "password332", "");
        userService.create("new_login4", "password332", (String) null);
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreateRoleEmpty(
    ) throws SQLException {
        userService.create("new_login4", "password332", (Role) null);
    }

    @Test
    public void testFindByLogin() throws SQLException {
        if (userList == null) return;
        Assert.assertEquals(userList.get(1), userService.findByLogin(userList.get(1).getLogin()));
    }

    @Test
    public void testFindByLoginNegative() throws SQLException {
        Assert.assertNull(userService.findByLogin("non-existent"));
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLoginEmpty() throws SQLException {
        if (userList == null) return;
        Assert.assertEquals(userList.get(1), userService.findByLogin(""));
        Assert.assertEquals(userList.get(0), userService.findByLogin(null));
    }

    @Test
    public void testFindByEmail() throws SQLException {
        if (userList == null) return;
        Assert.assertEquals(userList.get(1), userService.findByEmail(userList.get(1).getEmail()));
    }

    @Test
    public void testFindByEmailNegative() throws SQLException {
        Assert.assertNull(userService.findByEmail("non-existent"));
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmailEmpty() throws SQLException {
        if (userList == null) return;
        Assert.assertEquals(userList.get(1), userService.findByEmail(""));
        Assert.assertEquals(userList.get(0), userService.findByEmail(null));
    }

    @Test
    public void testRemoveOne() throws Exception {
        int size = userService.getSize();
        if (userList == null) return;
        Assert.assertEquals(userList.get(1), userService.removeOne(userList.get(1)));
        Assert.assertEquals(size - 1, userService.getSize());
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveOneNegative() throws Exception {
        userService.removeOne(null);
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        int size = userService.getSize();
        if (userList == null) return;
        @Nullable final UserDTO user = userService.findByLogin("admin2");
        Assert.assertEquals(user, userService.removeByLogin("admin2"));
        Assert.assertEquals(size - 1, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testRemoveByLoginEmptyLogin() throws Exception {
        userService.removeByLogin(null);
        userService.removeByLogin("");
    }

    @Test
    public void testRemoveByEmail() throws Exception {
        int size = userService.getSize();
        if (userList == null) return;
        @Nullable final UserDTO user = userService.findByEmail("user3@mail.ru");
        Assert.assertEquals(user, userService.removeByEmail("user3@mail.ru"));
        Assert.assertEquals(size - 1, userService.getSize());
    }

    @Test(expected = EmailEmptyException.class)
    public void testRemoveByLoginEmptyEmail() throws Exception {
        userService.removeByEmail(null);
        userService.removeByEmail("");
    }

    @Test
    public void testSetPassword() throws Exception {
        if (userList == null) return;
        Assert.assertNotNull(userService.setPassword(userList.get(0).getId(), "newPass2"));
    }

    @Test(expected = IdEmptyException.class)
    public void testSetPasswordEmptyId() throws Exception {
        userService.setPassword(null, "newPass");
        userService.setPassword("", "newPass");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetPasswordEmptyPassword() throws Exception {
        if (userList == null) return;
        userService.setPassword(userList.get(1).getId(), "");
        userService.setPassword(userList.get(1).getId(), null);
    }

    @Test
    public void testUpdateUser() throws Exception {
        @NotNull final String firstName = "new name";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        if (userList == null) return;
        @NotNull final String id = userList.get(0).getId();
        userService.updateUser(id, firstName, lastName, middleName);
        @NotNull final UserDTO user = userService.findOneById(id);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateNotFoundTask(
    ) throws Exception {
        @NotNull final String firstName = "new name";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        userService.updateUser("non-existent", firstName, lastName, middleName);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) throws Exception {
        userService.updateUser("", "firstName", "lastName", "middleName");
        userService.updateUser(null, "firstName", "lastName", "middleName");
    }

    @Test
    public void testIsLoginExist() throws SQLException {
        Assert.assertTrue(userService.isLoginExist("user"));
    }

    @Test
    public void testIsLoginExistNegative() throws SQLException {
        Assert.assertFalse(userService.isLoginExist("user4444"));
        Assert.assertFalse(userService.isLoginExist(""));
        Assert.assertFalse(userService.isLoginExist(null));
    }

    @Test
    public void testIsEmailExist() throws SQLException {
        Assert.assertTrue(userService.isEmailExist("user@user.ru"));
    }

    @Test
    public void testLockUserByLogin() throws SQLException {
        if (userList == null) return;
        @NotNull final UserDTO user = userService.lockUserByLogin(userList.get(0).getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByEmptyLogin() throws SQLException {
        userService.lockUserByLogin("");
        userService.lockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testLockUserByEmptyId() throws SQLException {
        userService.lockUserByLogin("non-existent");
    }

    @Test
    public void testUnlockUserByLogin() throws SQLException {
        if (userList == null) return;
        @NotNull final UserDTO user = userService.unlockUserByLogin(userList.get(0).getLogin());
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByEmptyLogin() throws SQLException {
        userService.unlockUserByLogin("");
        userService.unlockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testUnlockUserByEmptyId() throws SQLException {
        userService.unlockUserByLogin("non-existent");
    }

    @Test
    public void testFindOneById() throws Exception {
        if (userList == null) return;
        Assert.assertNotNull(userService.findOneById(userList.get(0).getId()));
        Assert.assertNotNull(userService.findOneById(userList.get(2).getId()));
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testFindOneByIdNegative() throws Exception {
        userService.findOneById("non-existent");
    }

}
