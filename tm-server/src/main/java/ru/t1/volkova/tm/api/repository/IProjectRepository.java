package ru.t1.volkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status})")
    int add(@NotNull ProjectDTO project) throws SQLException;

    @Update("UPDATE tm_project SET name = #{name}, created = #{created}, description = #{description}, " +
            "user_id = #{userId}, status = #{status} WHERE id = #{id}")
    void update(@NotNull ProjectDTO project) throws SQLException;

    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<ProjectDTO> findAlls() throws SQLException;

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<ProjectDTO> findAll(@Nullable @Param("userId") String userId) throws SQLException;

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<ProjectDTO> findAllByName(@Nullable @Param("userId") String userId) throws SQLException;

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<ProjectDTO> findAllByStatus(@Nullable @Param("userId") String userId) throws SQLException;

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<ProjectDTO> findAllByCreated(@Nullable @Param("userId") String userId) throws SQLException;

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    ProjectDTO findOneById(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id
    ) throws SQLException;

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    ProjectDTO findOneByIndex(
            @Nullable @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    ) throws SQLException;

    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@Nullable @Param("userId") String userId) throws SQLException;

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeOneById(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id
    ) throws SQLException;

    @Delete("DELETE FROM tm_project WHERE id = (SELECT id from tm_project WHERE " +
            "user_id = #{userId} LIMIT 1 OFFSET #{index})")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeOneByIndex(
            @Nullable @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    ) throws SQLException;

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeOne(@NotNull ProjectDTO project) throws SQLException;

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeAll(@Nullable @Param("userId") String userId) throws SQLException;

}
