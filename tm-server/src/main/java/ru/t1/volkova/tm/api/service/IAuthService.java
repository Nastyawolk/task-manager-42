package ru.t1.volkova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.SessionDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;

import java.sql.SQLException;

public interface IAuthService {

    @NotNull
    UserDTO registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email) throws SQLException;

    String login(
            @Nullable String login,
            @Nullable String password
    ) throws SQLException;

    @SneakyThrows
    SessionDTO validateToken(@Nullable String token);

    void invalidate(SessionDTO session) throws Exception;

}
