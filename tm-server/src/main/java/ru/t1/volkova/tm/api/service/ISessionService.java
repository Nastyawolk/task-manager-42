package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.SessionDTO;

import java.sql.SQLException;
import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO add(@NotNull SessionDTO session) throws SQLException;

    @Nullable
    SessionDTO removeOne(@NotNull SessionDTO session) throws SQLException;

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @NotNull
    SessionDTO findOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    List<SessionDTO> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    SessionDTO removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    SessionDTO removeOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    void removeAll(@Nullable String userId) throws SQLException;

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id) throws SQLException;

}
