package ru.t1.volkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, name, created, description, user_id, status, project_id)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status}, #{projectId})")
    int add(@NotNull TaskDTO task) throws SQLException;

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    List<TaskDTO> findAlls() throws SQLException;

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    List<TaskDTO> findAll(@Nullable @Param("userId") String userId) throws SQLException;

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    List<TaskDTO> findAllByName(@Nullable @Param("userId") String userId) throws SQLException;

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    List<TaskDTO> findAllByStatus(@Nullable @Param("userId") String userId) throws SQLException;

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    List<TaskDTO> findAllByCreated(@Nullable @Param("userId") String userId) throws SQLException;

    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    TaskDTO findOneById(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id
    ) throws SQLException;

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    TaskDTO findOneByIndex(
            @Nullable @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    ) throws SQLException;

    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@Nullable @Param("userId") String userId) throws SQLException;

    @Delete("DELETE FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeOneById(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id
    ) throws SQLException;

    @Delete("DELETE FROM tm_task WHERE id = (SELECT id from tm_task WHERE " +
            "user_id = #{userId} LIMIT 1 OFFSET #{index})")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeOneByIndex(
            @Nullable @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    ) throws SQLException;

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeOne(@NotNull TaskDTO task) throws SQLException;

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeAll(@Nullable @Param("userId") String userId
    ) throws SQLException;

    @Update("UPDATE tm_task SET name = #{name}, created = #{created}, description = #{description}, " +
            "user_id = #{userId}, status = #{status}, project_id = #{projectId} WHERE id = #{id}")
    void update(@NotNull TaskDTO task) throws SQLException;

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    ) throws SQLException;

}
