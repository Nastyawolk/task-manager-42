package ru.t1.volkova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.ISessionRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.ISessionService;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;
import ru.t1.volkova.tm.dto.model.SessionDTO;

import java.sql.SQLException;
import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @NotNull
    public SessionDTO add(@NotNull final SessionDTO session) throws SQLException {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.add(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Override
    @Nullable
    public SessionDTO removeOne(@NotNull final SessionDTO session) throws SQLException {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeOne(session);
            return session;
        }
    }

    @Override
    @Nullable
    public SessionDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findOneById(userId, id);
        }
    }

    @Override
    @NotNull
    public SessionDTO findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final SessionDTO session = repository.findOneByIndex(userId, index);
            if (session == null) throw new ProjectNotFoundException();
            return session;
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.getSize(userId);
        }
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findAll(userId);
        }
    }

    @Override
    @Nullable
    public SessionDTO removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final SessionDTO session;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneById(userId, id);
            repository.removeOneById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Override
    @Nullable
    public SessionDTO removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final SessionDTO session;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneByIndex(userId, index);
            repository.removeOneByIndex(userId, index);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @NotNull
    public Boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findOneById(userId, id) != null;
        }
    }

}
