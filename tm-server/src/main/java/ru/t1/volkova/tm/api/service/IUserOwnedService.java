package ru.t1.volkova.tm.api.service;

import ru.t1.volkova.tm.api.repository.IUserOwnedRepository;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedModel;

@SuppressWarnings("UnusedReturnValue")
public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {


}
