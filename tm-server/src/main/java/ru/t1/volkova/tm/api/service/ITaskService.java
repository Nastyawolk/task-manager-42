package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface ITaskService {

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws SQLException;

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws SQLException;

    @NotNull
    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull Status status
    ) throws SQLException;

    @Nullable
    TaskDTO changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @NotNull Status status
    ) throws SQLException;

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws SQLException;

    @Nullable
    List<TaskDTO> findAlls() throws SQLException;

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws SQLException;

    @NotNull
    TaskDTO add(@NotNull TaskDTO task) throws SQLException;

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<TaskDTO> findAllByName(@Nullable String userId) throws SQLException;

    @Nullable
    List<TaskDTO> findAllByStatus(@Nullable String userId) throws SQLException;

    @Nullable
    List<TaskDTO> findAllByCreated(@Nullable String userId) throws SQLException;

    @NotNull
    TaskDTO findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @NotNull
    TaskDTO findOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    TaskDTO removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    TaskDTO removeOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    @Nullable
    TaskDTO removeOne(@Nullable String userId, @NotNull TaskDTO task) throws SQLException;

    void removeAll(@Nullable String userId) throws SQLException;

    void update(@NotNull TaskDTO task) throws SQLException;
}
