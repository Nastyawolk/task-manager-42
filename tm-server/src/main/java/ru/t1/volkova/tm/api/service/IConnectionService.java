package ru.t1.volkova.tm.api.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {
    @NotNull
    SqlSession getSqlSession();

    @SneakyThrows
    SqlSessionFactory getSqlSessionFactory();

    @NotNull
    EntityManagerFactory factory();

}
