package ru.t1.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    boolean existsById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    List<M> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) throws SQLException;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    M findOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    M removeOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    @Nullable
    M removeOne(@Nullable String userId, @NotNull M model) throws SQLException;

    void removeAll(@Nullable String userId) throws SQLException;

    @Nullable
    M add(@Nullable String userId, @NotNull M model) throws SQLException;

}
