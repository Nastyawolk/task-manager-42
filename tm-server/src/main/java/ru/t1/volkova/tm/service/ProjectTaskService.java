package ru.t1.volkova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.api.service.IProjectTaskService;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.volkova.tm.exception.field.TaskIdEmptyException;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.sql.SQLException;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IConnectionService connectionService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.connectionService = connectionService;
    }

    @Override
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final TaskDTO task = taskService.findOneById(userId, taskId);
            task.setProjectId(projectId);
            taskService.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @Override
    public void removeProjectById(
            @NotNull final String userId,
            @Nullable final String projectId
    ) throws SQLException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks == null || tasks.size() == 0) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            for (@NotNull final TaskDTO task : tasks) {
                taskService.removeOneById(userId, task.getId());
            }
            @Nullable final ProjectDTO project = projectService.removeOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @Override
    @NotNull
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws SQLException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final TaskDTO task = taskService.findOneById(userId, taskId);
        try {
            task.setProjectId(null);
            taskService.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return task;
    }

}
