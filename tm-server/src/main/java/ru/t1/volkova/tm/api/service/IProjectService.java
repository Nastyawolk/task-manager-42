package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface IProjectService {

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description) throws SQLException;

    @NotNull
    ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description) throws SQLException;

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws SQLException;

    @NotNull
    ProjectDTO changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws SQLException;

    @Nullable
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws SQLException;

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    ProjectDTO findOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    List<ProjectDTO> findAlls() throws SQLException;

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<ProjectDTO> findAllByName(@Nullable String userId) throws SQLException;

    @Nullable
    List<ProjectDTO> findAllByStatus(@Nullable String userId) throws SQLException;

    @Nullable
    List<ProjectDTO> findAllByCreated(@Nullable String userId) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    ProjectDTO removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    ProjectDTO removeOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    @NotNull
    ProjectDTO removeOne(@Nullable String userId, @NotNull ProjectDTO model) throws SQLException;

    void removeAll(@Nullable String userId) throws SQLException;

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO model) throws SQLException;

}
