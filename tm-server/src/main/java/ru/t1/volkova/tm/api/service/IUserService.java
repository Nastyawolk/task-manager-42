package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.dto.model.UserDTO;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface IUserService {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password) throws SQLException;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws SQLException;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws SQLException;

    @Nullable List<UserDTO> findAll() throws SQLException;

    @Nullable
    UserDTO findByLogin(@Nullable String login) throws SQLException;

    @Nullable
    UserDTO findByEmail(@Nullable String email) throws SQLException;

    @NotNull
    UserDTO findOneById(@Nullable String id) throws SQLException;

    @NotNull
    UserDTO removeOne(@Nullable UserDTO model) throws Exception;

    @NotNull
    UserDTO removeByLogin(@Nullable String login) throws Exception;

    @NotNull
    UserDTO removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws Exception;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws SQLException;

    @NotNull
    Boolean isEmailExist(@Nullable String email) throws SQLException;

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login) throws SQLException;

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login) throws SQLException;

    int getSize() throws SQLException;
}
