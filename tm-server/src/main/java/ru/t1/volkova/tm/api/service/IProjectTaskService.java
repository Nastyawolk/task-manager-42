package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.sql.SQLException;

public interface IProjectTaskService {

    TaskDTO bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws Exception;

    void removeProjectById(@NotNull String userId, @Nullable String projectId) throws SQLException;

    @NotNull
    TaskDTO unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws SQLException;

}
